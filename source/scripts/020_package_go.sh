#!/usr/bin/env bash
GO_VERSION=1.14.3
GO_ARCH=linux-amd64
GO_ENDPOINT=https://dl.google.com/go
GO_DOWNLOAD_URL=$GO_ENDPOINT/go$GO_VERSION.$GO_ARCH.tar.gz
DEST=/var/tmp/packer-build/work

set -e

# download
mkdir -p $DEST
curl -L $GO_DOWNLOAD_URL > $DEST/golang.tar.gz

# install
tar -C /usr/local -xzvf $DEST/golang.tar.gz
rm -rf $DEST

# set path
echo 'PATH=$PATH:/usr/local/go/bin' > /etc/profile.d/go.sh
