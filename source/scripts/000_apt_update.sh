#!/usr/bin/env bash
apt-get update
apt-get install -y apt-transport-https ca-certificates software-properties-common 2>&1
apt-get -y install --no-install-recommends apt-utils dialog 2>&1
