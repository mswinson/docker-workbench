#!/usr/bin/env bash
set -e

groupadd $USERNAME
useradd --uid $USER_UID  --gid $USERNAME -m $USERNAME
echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME
chmod 0440 /etc/sudoers.d/$USERNAME
