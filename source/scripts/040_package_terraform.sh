#!/usr/bin/env bash
TERRAFORM_DOWNLOAD_ENDPOINT=https://releases.hashicorp.com/terraform
TERRAFORM_VERSION=0.12.26
TERRAFORM_ARCH=linux_amd64
TERRAFORM_DOWNLOAD_URL=${TERRAFORM_DOWNLOAD_ENDPOINT}/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_${TERRAFORM_ARCH}.zip

WORKDIR=/var/tmp/packer-build/work
DESTPATH=/usr/local/terraform

set -e
mkdir -p ${WORKDIR}

# download
curl -L ${TERRAFORM_DOWNLOAD_URL} > ${WORKDIR}/terraform.zip

# install
mkdir -p ${DESTPATH}
unzip ${WORKDIR}/terraform.zip -d ${DESTPATH}
rm -f ${WORKDIR}/terraform.zip

# set path
echo 'PATH=$PATH:/usr/local/terraform' > /etc/profile.d/terraform.sh
