#!/usr/bin/env bash
NODE_VERSION=14.x
NODE_ENDPOINT=https://deb.nodesource.com/
NODE_DOWNLOAD_URL=$NODE_ENDPOINT/setup_$NODE_VERSION
NPM_GLOBAL=/usr/local/share/npm-global
NVM_DIR=/usr/local/share/nvm
DEST=/var/tmp/packer-build/work

set -e

# install
mkdir -p $DEST
curl -sL $NODE_DOWNLOAD_URL | bash -
apt-get install -y nodejs
rm -rf $DEST

# set up npm
echo "setting up npm"
mkdir -p ${NPM_GLOBAL}
chown ${USERNAME}:root ${NPM_GLOBAL}
npm config -g set prefix ${NPM_GLOBAL}

# nvm
echo "setting up nvm"
export NVM_DIR
mkdir -p ${NVM_DIR}
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash

echo "source ${NVM_DIR}/nvm.sh" > /etc/profile.d/node.sh
echo "source ${NVM_DIR}/bash_completion" >> /etc/profile.d/node.sh
echo "export PATH=${NPM_GLOBAL}/bin:\${PATH}" >> /etc/profile.d/node.sh
chown -R ${USER_UID}:root ${NVM_DIR}


