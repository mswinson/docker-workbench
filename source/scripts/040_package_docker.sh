#!/usr/bin/env bash

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update
apt-get install -y docker-ce
apt-get install -y docker-compose
apt-get install -y socat

cp /var/tmp/packer-build/files/docker-init.sh /usr/local/share/docker-init.sh

touch /var/run/docker-host.sock
ln -s /var/run/docker-host.sock /var/run/docker.sock
chmod +x /usr/local/share/docker-init.sh
