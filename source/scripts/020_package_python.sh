#!/usr/bin/env bash
set -e

apt-get install -y python
apt-get install -y python-pip
apt-get install -y virtualenv
pip install cookiecutter
