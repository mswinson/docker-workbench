#!/usr/bin/env bash

apt-get install -y gnupg2

mkdir ~/.gnupg
touch ~/.gnupg/gpg.conf
echo "disable-ipv6" >> ~/.gnupg/dirmngr.conf
chmod 700 ~/.gnupg/
chmod 600 ~/.gnupg/*

