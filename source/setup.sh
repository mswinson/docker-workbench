#!/bin/sh
set -e
for f in /var/tmp/packer-build/scripts/*.sh
do
  chmod u+x $f
  sync
  $f
done

