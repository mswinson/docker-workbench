FROM ubuntu:18.04
MAINTAINER Mark Swinson <mark@mswinson.com>

ARG USERNAME=workbench
ARG USER_UID=1000
ARG USER_GID=1000

ENV DEBIAN_FRONTEND=noninteractive

ADD source /var/tmp/packer-build
RUN chmod u+x /var/tmp/packer-build/setup.sh && \
    sync && \
    /var/tmp/packer-build/setup.sh

ENTRYPOINT ["/usr/local/share/docker-init.sh", "workbench"]
CMD ["sleep", "infinity"]
USER workbench
WORKDIR /home/workbench

ENV DEBIAN_FRONTEND=dialog

