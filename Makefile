.PHONY: build clean smoke deploy release

IMAGE_REPO=mswinson
IMAGE_NAME=workbench
TAG=ubuntu
BUILDVERSION=`cat VERSION`
TAG_SUFFIX ?= '-develop'
TEST_CONTAINER_NAME ?= 'testwb'

build:
	docker build -t $(IMAGE_REPO)/$(IMAGE_NAME):$(TAG)-$(BUILDVERSION)$(TAG_SUFFIX) .

clean:
	docker rmi $(IMAGE_REPO)/$(IMAGE_NAME):$(TAG)-$(BUILDVERSION)$(TAG_SUFFIX)

smoke: build
	docker run -d --name $(TEST_CONTAINER_NAME) -u root $(IMAGE_REPO)/$(IMAGE_NAME):$(TAG)-$(BUILDVERSION)$(TAG_SUFFIX)
	docker cp ./test $(TEST_CONTAINER_NAME):/var/tmp/test
	docker exec $(TEST_CONTAINER_NAME) /var/tmp/test/smoke.sh
	docker stop $(TEST_CONTAINER_NAME)
	docker rm $(TEST_CONTAINER_NAME)
	
deploy:
	docker login --username $(DOCKER_USER) --password $(DOCKER_PASS)
	docker push $(IMAGE_REPO)/$(IMAGE_NAME):$(TAG)-$(BUILDVERSION)$(TAG_SUFFIX)

release:
	docker tag $(IMAGE_REPO)/$(IMAGE_NAME):$(TAG)-$(BUILDVERSION)$(TAG_SUFFIX) $(IMAGE_REPO)/$(IMAGE_NAME):latest
	docker tag $(IMAGE_REPO)/$(IMAGE_NAME):$(TAG)-$(BUILDVERSION)$(TAG_SUFFIX) $(IMAGE_REPO)/$(IMAGE_NAME):$(TAG)
	docker login --username $(DOCKER_USER) --password $(DOCKER_PASS)
	docker push $(IMAGE_REPO)/$(IMAGE_NAME):latest
	docker push $(IMAGE_REPO)/$(IMAGE_NAME):$(TAG)
