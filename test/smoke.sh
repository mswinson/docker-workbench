#!/bin/bash -l
set -e
curl --version
gpg2 --version
git --version
git flow version
go version
node --version
nvm --version
python --version
rvm --version
vim --version
docker --version
docker-compose --version
terraform -version
aws --version

# test user exists
id -u workbench
